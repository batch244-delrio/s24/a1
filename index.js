// console.log('helloMundo')

// Getting the cube

let num = 2
let cube = num ** 3;
let getCube = cube;

console.log(`The cube of ${num} is ${getCube}`);

// Var Address

let address = ['258', 'Washington Ave', 'NW', 'California 90011'];

let [hNumber, streetName, city, country] = address;
console.log(`I live at ${hNumber} ${streetName} ${city} ${country}`);


//Lolong

const animal = {
	name: 'Lolong',
	type: 'salt water crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in ',
};
const {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. he weighed at ${weight} with a measurement of ${length}.`);


// Num Array
let numArr = [1, 2, 3, 4, 5];
	numArr.forEach((number) => console.log(number));
let reduceNumber = numArr.reduce((x, y) => x + y);
console.log(reduceNumber);



// Class Dog
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let myDog = new Dog ('Frankie', 5, 'Miniature Dachshund');
console.log(myDog);